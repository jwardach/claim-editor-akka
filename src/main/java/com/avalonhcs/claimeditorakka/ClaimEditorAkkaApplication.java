package com.avalonhcs.claimeditorakka;

import akka.actor.ActorSystem;
import com.avalonhcs.claimeditorakka.config.SpringExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ClaimEditorAkkaApplication {

	@Autowired
	private ApplicationContext applicationContext;

	@Bean
	public ActorSystem actorSystem() {
		ActorSystem system = ActorSystem.create("claim-editor-demo");
		SpringExtension.SPRING_EXTENSION_PROVIDER.get(system).initialize(applicationContext);
		return system;
	}



	public static void main(String[] args) {
		SpringApplication.run(ClaimEditorAkkaApplication.class, args);
	}

}

package com.avalonhcs.claimeditorakka;


import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.avalonhcs.claimeditorakka.config.SpringExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Service
public class ClaimEditorWorkflow {

    @Autowired
    ActorSystem actorSystem;
    ActorRef resultsProcessorActorRef;

    @PostConstruct
    public void initialize() {
        resultsProcessorActorRef = actorSystem.actorOf(SpringExtension.SPRING_EXTENSION_PROVIDER.get(actorSystem).props("resultsProcessorActor"),"resultsProcessorActor");
    }

    public String processClaim(final String claimNumber) {
        /*
        Run Rules
         */
        System.out.println("Edited claim " + claimNumber + " on thread " + Thread.currentThread().getName());
        resultsProcessorActorRef.tell(claimNumber, resultsProcessorActorRef);
        return claimNumber;
    }

    @PreDestroy
    public void preDestroy() {
        actorSystem.terminate();
    }
}

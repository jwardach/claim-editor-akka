package com.avalonhcs.claimeditorakka.export;

import akka.actor.UntypedAbstractActor;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ResultsProcessorActor extends UntypedAbstractActor {


    @Override
    public void onReceive(Object message) throws Throwable, Throwable {
        System.out.println("Sending claim to queue " + message.toString()+ " on Thread: " + Thread.currentThread().getName());
    }
}

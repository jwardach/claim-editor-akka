package com.avalonhcs.claimeditorakka.integration;


import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.avalonhcs.claimeditorakka.ClaimEditorWorkflow;
import com.avalonhcs.claimeditorakka.config.SpringExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@RestController
public class ClaimController {

    @Autowired
    ClaimEditorWorkflow claimEditorWorkflow;


    @GetMapping("/claim")
    public String editClaim(@RequestParam("claim_number") String claimNumber) {
        System.out.println("Received claim " + claimNumber + " on thread " + Thread.currentThread().getName());
        return claimEditorWorkflow.processClaim(claimNumber);
    }

}
